package main

import (
	"./lib/logging"
	"./lib/xmlrpc"

	"github.com/go-mangos/mangos"
	"github.com/go-mangos/mangos/protocol/pair"
	"github.com/go-mangos/mangos/transport/tcp"
	"github.com/golang/protobuf/proto"
	"github.com/naoina/toml"
	"github.com/satori/go.uuid"

	"./util"

	"container/list"
	"encoding/xml"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

var (
	confFile      config
	isLog         logging.Log
	imAddress     map[string]addressThreadsChan
	methodThreads map[string]threadsChan
	globalTimeOut time.Duration
)

type interfaceMethods struct {
	Name    string `toml:"name"`
	Threads int    `toml:"threads"`
}

type cages struct {
	Address string             `toml:"address"`
	Threads int                `toml:"threads"`
	Methods []interfaceMethods `toml:"methods"`
}

type config struct {
	Global struct {
		AddressXMLRPC string `toml:"listen_address_xmlrpc"`
		AddressDMZ    string `toml:"listen_url_dmz"`
		TimeOut       int    `toml:"timeout"`
		Threads       int    `toml:"dmz"`
	} `toml:"global"`
	DMZ   []interfaceMethods `toml:"dmz"`
	Cages []cages            `toml:"cages"`
	Log   struct {
		File  string `toml:"logfile"`
		Info  bool   `toml:"info"`
		Error bool   `toml:"error"`
		Debug bool   `toml:"debug"`
	} `toml:"log"`
}

// для dmz
type threadsChan struct {
	channel    chan chan bool // канал для передачи управляющего канала стартующего выполнение запроса xmlrpc
	setThreads chan int       // канал для динамического изменения числа потоков счетчика, 0 - закрытие счетчика
}

// для локальных
type addressThreadsChan struct {
	address string
	threadsChan
}

// Данные обмена с DMZ
type dataForDMZ struct {
	channelReturn chan dataFromDMZ
	// chResetTimeOut chan bool
	uuid       string
	inter      string
	method     string
	inputURL   string
	data       []byte
	endTimeOut time.Time
}

type dataFromDMZ struct {
	uuid      string
	data      []byte
	errorCode int
	errorText string
}

// Handler
type handleWork struct {
	imAddress     *map[string]addressThreadsChan
	methodThreads *map[string]threadsChan
	TimeOut       *time.Duration  // Устанавливается в мсек.
	Channel       chan dataForDMZ // Канал для передачи данных для запроса на dmz клиенте
	ChannelUUID   chan string     // Канал для передачи uuid при срабатывании timeout для удаления из хранилича данных в serverDMZ
}

func (h handleWork) Work(interfaceMethod string, data []byte, inputURL string) (resultXML []byte, errorCode int, errorText string) {
	endTimeOut := time.Now().Add(*h.TimeOut)
	// isLog.Dbg(fmt.Sprintf("End timeout [%s]", endTimeOut))

	isLog.Inf(fmt.Sprintf("Start work: [%s]", interfaceMethod))

	arrayIM := strings.Split(interfaceMethod, ".")
	if len(arrayIM) != 2 {
		errorCode = http.StatusBadRequest
		errorText = fmt.Sprintf("Bad format \"MethodName\" into request: [%s]", interfaceMethod)
		isLog.Err(errorText)
		return
	}

	isLog.Dbg(fmt.Sprintf("URL [%s]", inputURL))

	// Проверяем на соответствие разрешенных интерфейсов
	localAddress, okLocal := (*h.imAddress)[interfaceMethod]
	methodDMZ, okDMZ := (*h.methodThreads)[arrayIM[0]]
	isLog.Dbg(fmt.Sprintf("localAddress [%v], methodDMZ [%v]", okLocal, okDMZ))
	if !okLocal && !okDMZ {
		errorCode = http.StatusForbidden
		errorText = fmt.Sprintf("Not permitted cage [%s]", interfaceMethod)
		isLog.Err(errorText)
		return
	}

	// Локальный вызов функций
	if okLocal {
		duration := endTimeOut.Sub(time.Now())
		isLog.Inf(fmt.Sprintf("It is local call [%s]", interfaceMethod))
		chSync := make(chan bool)
		localAddress.channel <- chSync

		// ожидание разрешения
		isLog.Inf(fmt.Sprintf("Waiting local start for [%s]", interfaceMethod))
		<-chSync
		isLog.Inf(fmt.Sprintf("Local start for [%s]", interfaceMethod))

		// выполнение работы
		addressURL := localAddress.address
		if inputURL != "" {
			addressURL += "?" + inputURL
		}

		// Собираем XML
		methodCall := xmlrpc.MethodCall{MethodName: interfaceMethod, Result: data}
		xmlOut, _ := xml.Marshal(&methodCall)

		// Вызываем клиента
		isLog.Inf(fmt.Sprintf("Call local [%s] on [%s] with duration [%s]", interfaceMethod, addressURL, duration))
		response, err := xmlrpc.Client(addressURL, interfaceMethod, xmlOut, duration)
		isLog.Dbg(fmt.Sprintf("Response local [%s], response [%+v]", interfaceMethod, response))

		if err != nil {
			errorCode = http.StatusInternalServerError
			errorText = err.Error()
			isLog.Err(errorText)
		} else {
			resultXML, err = ioutil.ReadAll(response.Body)
			if err != nil {
				errorCode = http.StatusInternalServerError
				errorText = fmt.Sprintf("Error body local [%s] from response [%s]", interfaceMethod, err)
				isLog.Err(errorText)
			} else {
				isLog.Dbg(fmt.Sprintf("Result from local [%s], [%s]", interfaceMethod, resultXML))
			}
		}

		isLog.Inf(fmt.Sprintf("Local stop for [%s]", interfaceMethod))

		localAddress.channel <- nil
	}

	if !okLocal && okDMZ {
		// Передача для вызова в DMZ
		isLog.Inf(fmt.Sprintf("It is dmz call [%s]", interfaceMethod))
		chSync := make(chan bool)
		methodDMZ.channel <- chSync
		// Создаем uuid
		uuid := uuid.NewV4()
		isLog.Inf(fmt.Sprintf("%s: Create uuid for [%s]", uuid, interfaceMethod))

		// Подготовка данных для передачи
		chFromDMZ := make(chan dataFromDMZ)
		data := dataForDMZ{
			channelReturn: chFromDMZ,
			uuid:          uuid.String(),
			inter:         arrayIM[0],
			method:        arrayIM[1],
			inputURL:      inputURL,
			data:          data,
			endTimeOut:    endTimeOut,
		}

		// ожидание разрешения
		isLog.Inf(fmt.Sprintf("%s: Waiting dmz start for [%s]", uuid, interfaceMethod))
		<-chSync
		isLog.Inf(fmt.Sprintf("%s: DMZ start for [%s]", uuid, interfaceMethod))

		// выполнение работы
		h.Channel <- data
		isLog.Inf(fmt.Sprintf("%s: Send data for prepare", uuid))

		var result dataFromDMZ

		// Ожидание данных от DMZ
		select {
		case result = <-chFromDMZ:
			resultXML = result.data
		case <-time.After(endTimeOut.Sub(time.Now())):
			isLog.Dbg(fmt.Sprintf("%s: End timeout", uuid))
			errorCode = http.StatusRequestTimeout
			errorText = fmt.Sprint("Timeout expire")
			isLog.Err(fmt.Sprintf("%s: %s", uuid, errorText))
			close(chFromDMZ)
			h.ChannelUUID <- uuid.String()
		}
		isLog.Inf(fmt.Sprintf("%s: DMZ stop for [%s]", uuid, interfaceMethod))

		methodDMZ.channel <- nil
		isLog.Inf(fmt.Sprintf("%s: DMZ send to counter for [%s]", uuid, interfaceMethod))
	}

	return
}

// Выгребание из стека
func getStake(l *list.List, count, max int) *list.Element {
	if count < max {
		e := l.Front()
		l.Remove(e)
		return e
	}
	return nil
}

// Счетчик потоков для вызовов
func counter(name string, threads int, chDMZ chan chan bool, startSendDMZ chan bool, setThreads chan int) {
	// стек каналов от xmlrpc запросов, очередь запросов xmlrpc на выполнение
	inputChan := list.New()
	// число выполняемых запросов
	count := 0
	// максимальное число запросов
	maxThreads := threads

	for {
		select {
		case c := <-chDMZ:
			isLog.Inf(fmt.Sprintf("DMZ receive to counter for [%s], cannel [%v]", name, c))
			if startSendDMZ != nil {
				// Стартует передачу данных в dmz
				isLog.Dbg("Send sign to startSendDMZ")
				startSendDMZ <- true
			}
			if c != nil {
				inputChan.PushBack(c)
			} else {
				// окончание запроса
				count--
			}
			var e *list.Element
			if inputChan.Len() > 0 {
				e = getStake(inputChan, count, maxThreads)
			}
			if e != nil {
				// запуск запроса на выполнение
				e.Value.(chan bool) <- true
				count++
			}
		case maxThreads = <-setThreads:
		}
		isLog.Dbg(fmt.Sprintf("Current threads for [%s] is [%d]", name, count))
		if maxThreads == 0 && count == 0 {
			isLog.Dbg(fmt.Sprintf("Stop counter for [%s]", name))
			break
		}
	}
}

// Посылка данных в DMZ
func getAndSend(sock mangos.Socket, uuids *list.List, datas map[string]dataForDMZ) /*(ok bool)*/ {
	ok := false
	var dataDMZ dataForDMZ
	var uuid string

	for !ok && uuids.Len() != 0 {
		e := uuids.Front()
		if e != nil {
			uuids.Remove(e)
			uuid = e.Value.(string)
			dataDMZ, ok = datas[uuid]
		}
	}
	var err error
	if ok {
		// Упаковка в protoBuf
		var dataPB []byte
		duration := dataDMZ.endTimeOut.Sub(time.Now())
		dataPB, err = proto.Marshal(&util.Send{
			Uuid:      proto.String(uuid),
			Interface: proto.String(dataDMZ.inter),
			Method:    proto.String(dataDMZ.method),
			Params:    proto.String(dataDMZ.inputURL),
			Data:      dataDMZ.data,
			Duration:  proto.String(duration.String()),
		})
		if err != nil {
			isLog.Err(fmt.Sprintf("%s: Can't packing data for DMZ [%s]", uuid, err))
			return
		}
		isLog.Inf(fmt.Sprintf("%s: Packing data to DMZ", uuid))

		sock.SetOption(mangos.OptionSendDeadline, duration)
		err = sock.Send(dataPB)
		if err != nil {
			isLog.Err(fmt.Sprintf("%s: Can't send data to DMZ [%s]", uuid, err))
		} else {
			isLog.Inf(fmt.Sprintf("%s: Send data to DMZ", uuid))
		}
	}
	return
}

// Обработчик ответов на запросы от dmz клиента
func serverDMZ(sock mangos.Socket, channel chan dataForDMZ, startSendDMZ chan bool, channelUUID chan string) {
	containerReturnChannels := make(map[string]chan dataFromDMZ)
	channelDMZ := make(chan []byte)
	uuids := list.New()                  // стек
	datas := make(map[string]dataForDMZ) // хранилище данных

	// Запуск слушателя запросов
	go func(sock mangos.Socket, ch chan<- []byte) {
		for {
			msg, err := sock.Recv()
			if err != nil {
				isLog.Err(fmt.Sprintf("Don't receive data from DMZ: [%s]", err))
			} else {
				isLog.Inf("Receive data from DMZ")
				ch <- msg
			}
		}
	}(sock, channelDMZ)

	for {
		// isLog.Dbg(fmt.Sprintf("serverDMZ 0, length [%d]", len(datas)))
		select {
		// Получение данных для DMZ
		case data := <-channel:

			// Сохранение канала для возврата данных в словарь по uuid
			containerReturnChannels[data.uuid] = data.channelReturn
			// Сохранение uuid в стеке
			uuids.PushBack(data.uuid)
			// Сохранение данных для передачи
			datas[data.uuid] = data
			isLog.Inf(fmt.Sprintf("%s: Prepare data", data.uuid))
			// Передача данных для выполнение запроса
			getAndSend(sock, uuids, datas)

			// Срабатываение timeout для определенного uuid
		case endUUID := <-channelUUID:
			isLog.Dbg(fmt.Sprintf("%s: Delete prepare data on timeout", endUUID))
			// Удаление данных
			delete(datas, endUUID)

			// Получение данных от DMZ
		case data := <-channelDMZ:
			isLog.Dbg(fmt.Sprintf("Receive from DMZ, length [%d]", len(data)))
			// Распаковываем protoBuff
			dataPB := &util.Receive{}
			err := proto.Unmarshal(data, dataPB)
			if err != nil {
				isLog.Err(fmt.Sprintf("Don't unpacking data [%s]", err))
			} else {
				// Передача данных в ожидающий сервер
				uuid := dataPB.GetUuid()
				dataDMZ := dataFromDMZ{uuid: uuid, data: dataPB.GetResult(), errorCode: int(dataPB.GetErrorCode()), errorText: dataPB.GetErrorText()}
				isLog.Inf(fmt.Sprintf("%s: Prepare data for send to back", dataDMZ.uuid))
				if returnData, ok := datas[dataDMZ.uuid]; ok {

					// возвращает данные
					// returnData.channelReturn <- dataDMZ  -- это было
					// сейчас вызов через функцию с обработкой закрытого канала
					sendDataToBack(returnData.channelReturn, dataDMZ)
					// Удаление данных
					delete(datas, uuid)
					isLog.Inf(fmt.Sprintf("%s: Send data to back", dataDMZ.uuid))
				} else {
					isLog.Err(fmt.Sprintf("%s: Haven't data to back", dataDMZ.uuid))
				}
			}

			// Передача данных для выполнение запроса
		case <-startSendDMZ:
			isLog.Dbg("Receive sign from startSendDMZ")
			getAndSend(sock, uuids, datas)
		}
	}
}

func sendDataToBack(ch chan dataFromDMZ, data dataFromDMZ) {
	// посылка в канал который может быть закрыт
	defer func() {
		if err := recover(); err != nil {
			isLog.Dbg(fmt.Sprintf("%s: Data remove because timeout", data.uuid))
		}
	}()
	ch <- data
}

func setConfig(configName string, reload bool, startSendDMZ chan bool) (err error) {
	var buf []byte
	var f *os.File
	var textError string

	defer func() {
		if textError != "" {
			if reload {
				err = errors.New(textError)
				return
			} else {
				log.Panicln(textError)
			}
		}
		return
	}()

	f, err = os.Open(configName)
	if err != nil {
		textError = fmt.Sprintf("Error open config file [%s]: [%s]", configName, err)
		return
	}
	defer f.Close()

	buf, err = ioutil.ReadAll(f)
	if err != nil {
		textError = fmt.Sprintf("Error read config file: [%s]", err)
		return
	}

	var configTmp config
	if err = toml.Unmarshal(buf, &configTmp); err != nil {
		textError = fmt.Sprintf("Error into config file: [%s]", err)
		return
	}
	confFile = configTmp

	if confFile.Log.File == "" {
		confFile.Log.File = "dmz_server.log"
	}

	level := 0
	if confFile.Log.Info {
		level |= logging.Linfo
	}
	if confFile.Log.Error {
		level |= logging.Lerror
	}
	if confFile.Log.Debug {
		level |= logging.Ldebug
	}

	if reload {
		isLog.Change(confFile.Log.File, level)
	} else {
		isLog = logging.New(confFile.Log.File, level)

		log.SetFlags(log.Lshortfile | log.Lmicroseconds)
	}

	// Подготовка данных для локальных вызовов
	imAddressTmp := make(map[string]addressThreadsChan)
	for _, cage := range confFile.Cages {
		for _, method := range cage.Methods {
			if _, ok := imAddressTmp[method.Name]; ok {
				isLog.Err(fmt.Sprintf("Duplicate interface/method [%s]", method.Name))
				continue
			}
			var threads int
			if method.Threads != 0 {
				threads = method.Threads
			} else if cage.Threads != 0 {
				threads = cage.Threads
			} else {
				threads = confFile.Global.Threads
			}
			if threads == 0 {
				log.Panicf("Haven't any threads for [%s]\n", method.Name)
			}

			var ok bool
			if imAddressTmp[method.Name], ok = imAddress[method.Name]; ok {
				imAddressTmp[method.Name].setThreads <- threads
				imAddressTmp[method.Name] = addressThreadsChan{address: cage.Address, threadsChan: threadsChan{channel: imAddressTmp[method.Name].channel, setThreads: imAddressTmp[method.Name].setThreads}}

			} else {
				channel := make(chan chan bool)
				setThreads := make(chan int)
				imAddressTmp[method.Name] = addressThreadsChan{address: cage.Address, threadsChan: threadsChan{channel: channel, setThreads: setThreads}}
				// Запуск счетчика потоков
				go counter(method.Name, threads, channel, nil, setThreads)
			}
		}
	}
	deadCounter := []chan int{}
	for k, v := range imAddress {
		if _, ok := imAddressTmp[k]; !ok {
			deadCounter = append(deadCounter, v.setThreads)
			isLog.Dbg(fmt.Sprintf("Dead counter [%s]", k))
		}
	}
	isLog.Dbg(fmt.Sprintf("Amount dead local counters [%d]", len(deadCounter)))

	imAddress = imAddressTmp

	// Закапывание мертвых счетчиков
	for _, ch := range deadCounter {
		ch <- 0
	}

	isLog.Dbg(fmt.Sprintf("Local cages for interface.method [%+v]", imAddress))

	// Подготовка данных для передачи в dmz

	methodThreadsTmp := make(map[string]threadsChan) // Канал для запуска передачи данных в dmz

	for _, dmz := range confFile.DMZ {
		if _, ok := methodThreadsTmp[dmz.Name]; ok {
			isLog.Err(fmt.Sprintf("Duplicate interface [%s]", dmz.Name))
			continue
		}
		var threads int
		if dmz.Threads != 0 {
			threads = dmz.Threads
		} else {
			threads = confFile.Global.Threads
		}
		if threads == 0 {
			log.Panicf("Haven't any threads for [%s]\n", dmz.Name)
		}

		var ok bool
		if methodThreadsTmp[dmz.Name], ok = methodThreads[dmz.Name]; ok {
			methodThreadsTmp[dmz.Name].setThreads <- threads
		} else {
			channel := make(chan chan bool)
			setThreads := make(chan int)
			methodThreadsTmp[dmz.Name] = threadsChan{channel: channel, setThreads: setThreads}
			// Запуск счетчика потоков
			go counter(dmz.Name, threads, channel, startSendDMZ, setThreads)
		}
	}
	deadCounter = []chan int{}
	for k, v := range methodThreads {
		if _, ok := methodThreadsTmp[k]; !ok {
			deadCounter = append(deadCounter, v.setThreads)
			isLog.Dbg(fmt.Sprintf("Dead counter [%s]", k))
		}
	}
	isLog.Dbg(fmt.Sprintf("Amount dead dmz counters [%d]", len(deadCounter)))

	methodThreads = methodThreadsTmp

	// Закапывание мертвых счетчиков
	for _, ch := range deadCounter {
		ch <- 0
	}

	isLog.Dbg(fmt.Sprintf("DMZ for interfaces [%+v]", methodThreads))

	globalTimeOut = time.Millisecond * time.Duration(confFile.Global.TimeOut)
	isLog.Dbg(fmt.Sprintf("Set timeout [%s]", globalTimeOut))

	return
}

func main() {
	configName := "dmz_server.conf"
	if len(os.Args) > 1 {
		configName = os.Args[1]
	}

	// канал для передачи сигнала для передачи данных в dmz
	startSendDMZ := make(chan bool)

	setConfig(configName, false, startSendDMZ)
	defer isLog.Close()
	var err error

	// Создание сокета для ожидания запросов от dmz клиента
	var sock mangos.Socket
	if sock, err = pair.NewSocket(); err != nil {
		log.Panicf("Can't get new rep socket: [%s]\n", err)
	}
	defer sock.Close()
	sock.AddTransport(tcp.NewTransport())
	if err = sock.Listen(confFile.Global.AddressDMZ); err != nil {
		log.Panicf("Can't listen on rep socket: %s", err)
	}

	// Перегрузчик конфига
	sigs := make(chan os.Signal)
	signal.Notify(sigs, syscall.SIGHUP)

	go func() {
		for {
			<-sigs
			err := setConfig(configName, true, startSendDMZ)
			if err != nil {
				isLog.Err(fmt.Sprintf("Can't reconfigure [%s]", err))
			} else {
				isLog.Inf("Reconfigure")
			}
		}
	}()

	chDMZ := make(chan dataForDMZ)
	channelUUID := make(chan string)

	// Запуск обработчика ответов на запросы от dmz клиента
	go serverDMZ(sock, chDMZ, startSendDMZ, channelUUID)

	server := xmlrpc.XmlRpcServer{
		Worker: handleWork{
			imAddress:     &imAddress,
			methodThreads: &methodThreads,
			TimeOut:       &globalTimeOut,
			Channel:       chDMZ,
			ChannelUUID:   channelUUID,
		}}

	http.Handle("/", server)

	isLog.Inf(fmt.Sprintf("Listen address for XMLRPC [%s]", confFile.Global.AddressXMLRPC))

	log.Fatal(http.ListenAndServe(confFile.Global.AddressXMLRPC, nil))

}
