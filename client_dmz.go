package main

import (
	"./lib/logging"
	"./lib/xmlrpc"

	"github.com/go-mangos/mangos"
	"github.com/go-mangos/mangos/protocol/pair"
	"github.com/go-mangos/mangos/transport/tcp"
	"github.com/golang/protobuf/proto"
	"github.com/naoina/toml"

	"./util"

	"encoding/xml"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

var (
	confFile config
	isLog    logging.Log
	cageAddr map[string]string
)

type cages struct {
	Address string   `toml:"address"`
	Names   []string `toml:"names"`
}

type config struct {
	Global struct {
		AddressDMZ string `toml:"url_dmz"`
	} `toml:"global"`
	Cages []cages `toml:"cages"`
	Redis struct {
		Address string `toml:"address"`
		Network string `toml:"network"`
	} `toml:"redis"`
	Log struct {
		File  string `toml:"logfile"`
		Info  bool   `toml:"info"`
		Error bool   `toml:"error"`
		Debug bool   `toml:"debug"`
	} `toml:"log"`
}

func client(cageAddr map[string]string, msg []byte, channel chan []byte) {
	var errorCode int
	var errorText string
	var result []byte

	uuid := "none"
	dataPB := &util.Send{}
	err := proto.Unmarshal(msg, dataPB)
	if err != nil {
		errorCode = http.StatusInternalServerError
		errorText = fmt.Sprintf("Don't unpacking data [%s]", err)
		isLog.Err(errorText)
	} else {
		var duration time.Duration
		duration, err = time.ParseDuration(dataPB.GetDuration())
		if err != nil {
			isLog.Err(fmt.Sprintf("Don't get timeout [%s]", err))
		}

		uuid = dataPB.GetUuid()
		isLog.Inf(fmt.Sprintf("%s: Get data for [%s.%s], timeout [%s]", uuid, dataPB.GetInterface(), dataPB.GetMethod(), duration))

		// Проверяем на соответствие разрешенных интерфейсов
		address, ok := cageAddr[dataPB.GetInterface()]
		if !ok {
			errorCode = http.StatusInternalServerError
			errorText = fmt.Sprintf("Not this interface in config: [%s]", dataPB.GetInterface())
			isLog.Err(fmt.Sprintf("%s: %s", uuid, errorText))
		}
		// Формируем адресную строку
		if dataPB.GetParams() != "" {
			address = fmt.Sprintf("%s?%s", address, dataPB.GetParams())
		}
		// Собираем XML
		interfaceMethod := fmt.Sprintf("%s.%s", dataPB.GetInterface(), dataPB.GetMethod())
		methodCall := xmlrpc.MethodCall{MethodName: interfaceMethod, Result: dataPB.GetData()}
		xmlOut, _ := xml.Marshal(&methodCall)

		// Передаем метод в pythomnic3k
		var response *http.Response
		isLog.Inf(fmt.Sprintf("%s: Call [%s] on [%s], data [%s], duration [%s]", uuid, interfaceMethod, address, xmlOut, duration))
		response, err = xmlrpc.Client(address, interfaceMethod, xmlOut, duration)
		if err != nil {
			isLog.Err(fmt.Sprintf("%s: [%s]", uuid, err))
			errorCode = http.StatusInternalServerError
			errorText = err.Error()
		} else {
			isLog.Dbg(fmt.Sprintf("%s: Response [%+v]", uuid, response))
			body, err := ioutil.ReadAll(response.Body)
			if err != nil {
				errorCode = http.StatusInternalServerError
				errorText = fmt.Sprintf("Error body from response [%s]", err)
				isLog.Err(fmt.Sprintf("%s: %s", uuid, errorText))
			} else {
				result = body
			}
			isLog.Dbg(fmt.Sprintf("%s: Body [%s]", uuid, result))
		}
	}
	if errorCode != 0 {
		codeE := xmlrpc.FaultMember{Name: "faultCode", Int: errorCode}
		textE := xmlrpc.FaultMember{Name: "faultString", String: errorText}
		responseXml := xmlrpc.MethodResponse{FaultMembers: []xmlrpc.FaultMember{codeE, textE}}
		xmlOut, _ := xml.Marshal(&responseXml)
		result = xmlOut
	}
	isLog.Dbg(fmt.Sprintf("%s: Result [%s]", uuid, result))

	// Упаковываем в protoBuf
	msg, err = proto.Marshal(&util.Receive{
		Uuid:      proto.String(uuid),
		Result:    result,
		ErrorCode: proto.Uint32(uint32(errorCode)),
		ErrorText: proto.String(errorText),
	})
	channel <- msg
}

func setConfig(configName string, reload bool) (err error) {
	var buf []byte
	var f *os.File
	var textError string

	defer func() {
		if textError != "" {
			if reload {
				err = errors.New(textError)
				return
			} else {
				log.Panicln(textError)
			}
		}
		return
	}()

	f, err = os.Open(configName)
	if err != nil {
		textError = fmt.Sprintf("Error open config file [%s]: [%s]", configName, err)
		return
	}
	defer f.Close()

	buf, err = ioutil.ReadAll(f)
	if err != nil {
		textError = fmt.Sprintf("Error read config file: [%s]", err)
		return
	}

	var configTmp config
	if err = toml.Unmarshal(buf, &configTmp); err != nil {
		textError = fmt.Sprintf("Error into config file: [%s]", err)
		return
	}
	confFile = configTmp

	if confFile.Log.File == "" {
		confFile.Log.File = "dmz_client.log"
	}

	level := 0
	if confFile.Log.Info {
		level |= logging.Linfo
	}
	if confFile.Log.Error {
		level |= logging.Lerror
	}
	if confFile.Log.Debug {
		level |= logging.Ldebug
	}

	if reload {
		isLog.Change(confFile.Log.File, level)
	} else {
		isLog = logging.New(confFile.Log.File, level)

		log.SetFlags(log.Lshortfile | log.Lmicroseconds)
	}

	// Соответствие интерфейсов адресам
	var interf []string
	cageAddrTmp := make(map[string]string)
	for _, cg := range confFile.Cages {
		for _, n := range cg.Names {
			if _, ok := cageAddrTmp[n]; ok {
				isLog.Err(fmt.Sprintf("Duplicate name interface [%s]", n))
				continue
			}
			cageAddrTmp[n] = cg.Address
			interf = append(interf, n)
		}
	}
	cageAddr = cageAddrTmp
	isLog.Dbg(fmt.Sprintf("Interfaces [%+v], addresses cages [%+v]", interf, cageAddr))
	return
}

func main() {
	configName := "dmz_client.conf"
	if len(os.Args) > 1 {
		configName = os.Args[1]
	}

	setConfig(configName, false)
	defer isLog.Close()
	var err error

	// Создание сокета для запросов к dmz серверу
	var sock mangos.Socket
	if sock, err = pair.NewSocket(); err != nil {
		log.Panicf("Can't get new rep socket: [%s]\n", err)
	}
	defer sock.Close()
	sock.AddTransport(tcp.NewTransport())
	if err = sock.Dial(confFile.Global.AddressDMZ); err != nil {
		log.Panicf("Can't dial on req socket: %s", err)
	}

	// Клиент DMZ

	isLog.Inf("Start client")

	chForDmz := make(chan []byte)

	go func(chForDmz chan []byte) {
		var msg []byte
		var err error
		for {
			// Получение данных
			msg, err = sock.Recv()
			if err != nil {
				isLog.Err(fmt.Sprintf("Can't receive date [%s]", err))
			}

			isLog.Dbg(fmt.Sprintf("Receive data, length [%d]", len(msg)))
			go client(cageAddr, msg, chForDmz)
		}
	}(chForDmz)

	// Перегрузчик конфига
	sigs := make(chan os.Signal)
	signal.Notify(sigs, syscall.SIGHUP)

	go func() {
		for {
			<-sigs
			err := setConfig(configName, true)
			if err != nil {
				isLog.Err(fmt.Sprintf("Can't reconfigure [%s]", err))
			} else {
				isLog.Inf("Reconfigure")
			}
		}
	}()

	var msg []byte
	// Сервер
	for {
		msg = <-chForDmz
		// Посылка данных на сервер
		err = sock.Send(msg)
		if err != nil {
			isLog.Err(fmt.Sprintf("Don't send data [%s]", err))
		} else {
			isLog.Inf("Send data")
		}
	}
}
