package logging

import (
	"errors"
	"fmt"
	"log"
	"os"
)

type Log struct {
	info    *log.Logger
	debug   *log.Logger
	error   *log.Logger
	logFile *os.File
}

const (
	Linfo = 1 << iota
	Lerror
	Ldebug
)

func New(name string, level int) Log {
	isLog := Log{}
	var err error
	isLog.logFile, err = os.OpenFile(name, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Panicf("Error create log file [%s]: [%s]\n", name, err)
	}

	if level&Linfo == Linfo {
		isLog.info = log.New(isLog.logFile, "INF: ", log.Lmicroseconds|log.Ldate)
	}
	if level&Lerror == Lerror {
		isLog.error = log.New(isLog.logFile, "ERR: ", log.Lmicroseconds|log.Ldate)
	}
	if level&Ldebug == Ldebug {
		isLog.debug = log.New(isLog.logFile, "DBG: ", log.Lmicroseconds|log.Ldate)
	}
	return isLog
}

func (l *Log) Change(name string, level int) (err error) {
	var newFile *os.File
	newFile, err = os.OpenFile(name, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		err = errors.New(fmt.Sprintf("Don't change config [%s]", err))
		return
	}
	if level&Linfo == Linfo {
		if l.info == nil {
			l.info = log.New(newFile, "INF: ", log.Lmicroseconds|log.Ldate)
		} else {
			l.info.SetOutput(newFile)
		}
	} else {
		l.info = nil
	}

	if level&Lerror == Lerror {
		if l.error == nil {
			l.error = log.New(newFile, "ERR: ", log.Lmicroseconds|log.Ldate)
		} else {
			l.error.SetOutput(newFile)
		}
	} else {
		l.error = nil
	}

	if level&Ldebug == Ldebug {
		if l.debug == nil {
			l.debug = log.New(newFile, "DBG: ", log.Lmicroseconds|log.Ldate)
		} else {
			l.debug.SetOutput(newFile)
		}
	} else {
		l.debug = nil
	}

	l.logFile.Close()
	l.logFile = newFile
	return
}

func (l Log) Close() {
	l.logFile.Close()
}

func (l Log) Inf(message string) {
	if l.info != nil {
		l.info.Print(message)
	}
}

func (l Log) Err(message string) {
	if l.error != nil {
		l.error.Print(message)
	}
}

func (l Log) Dbg(message string) {
	if l.debug != nil {
		l.debug.Print(message)
	}
}
