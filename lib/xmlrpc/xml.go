package xmlrpc

import (
	"encoding/xml"
)

type MethodCall struct {
	// Структура вызова метода XMLRPC.
	// Входной параметр только строка, т.к. других у нас не встречал.
	XMLName    xml.Name `xml:"methodCall"`
	MethodName string   `xml:"methodName"`
	Result     []byte   `xml:"params>param>value>string"`
}

type FaultMember struct {
	// Ошибка работы метода XMLRPC.
	XMLName xml.Name `xml:"member"`
	Name    string   `xml:"name"`
	String  string   `xml:"value>string,omitempty"`
	Int     int      `xml:"value>int,omitempty"`
}

type MethodResponse struct {
	// Структура результата работы метода XMLRPC.
	// Вариант с ошибками.
	XMLName      xml.Name      `xml:"methodResponse"`
	FaultMembers []FaultMember `xml:"fault>value>struct>member,omitempty"`
	Result       string        `xml:"params>param>value>string,omitempty"`
}

type MethodResponseSuccess struct {
	// Структура результата работы метода XMLRPC.
	// Вариант с ошибками.
	XMLName xml.Name `xml:"methodResponse"`
	Result  string   `xml:"params>param>value>string,omitempty"`
}

type RequestType struct {
	// Структура внутреннего метода XMLRPC.
	// Необходима только для определения типа запроса.
	// Используется только для тестовой программы.
	XMLName xml.Name `xml:"request"`
	Type    string   `xml:"type,attr"`
}

type UrlParamsData struct {
	// Для передачи данных через Redis
	Params string
	// Параметры URL
	Data []byte
	// Данные
}
