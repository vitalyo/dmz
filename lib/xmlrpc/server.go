package xmlrpc

import (
	"golang.org/x/net/html/charset"

	"encoding/xml"
	"fmt"
	"net/http"
)

type Worker interface {
	// Функция реализующая обработку запроса XMLRPC.
	// interfaceMethod - массив из 2 строк,
	// первая строка - имя интерфейса, вторая - имя метода,
	// так сделано для pythomnic3k.
	// data - данные передаваемые pythomnic3k-ому методу XMLRPC.
	// url - URL вызова
	Work(interfaceMethod string, data []byte, inputURL string) (resultXML []byte, errorCode int, errorText string)
}

type XmlRpcServer struct {
	// Обработчик для http.Handle
	RequestXml MethodCall
	Worker     Worker
}

/*
Собственно реализует обработку HTTP запросов.
*/
func (s XmlRpcServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var resultXML []byte
	var errorText string
	var errorCode int
	defer func() {
		if errorCode != 0 {
			codeE := FaultMember{Name: "faultCode", Int: errorCode}
			textE := FaultMember{Name: "faultString", String: errorText}
			responseXml := MethodResponse{FaultMembers: []FaultMember{codeE, textE}}
			xmlOut, _ := xml.Marshal(&responseXml)
			// fmt.Printf("xmlOut [%s]\n", xmlOut)
			fmt.Fprint(w, fmt.Sprintf(`<?xml version="1.0" encoding="utf-8"?>%s`, xmlOut))
		} else {
			// fmt.Printf("resultXML [%s]\n", resultXML)
			fmt.Fprintf(w, "%s", resultXML)
		}
	}()

	w.Header().Set("Content-Type", "text/xml")

	decoder := xml.NewDecoder(r.Body)
	decoder.CharsetReader = charset.NewReaderLabel
	err := decoder.Decode(&s.RequestXml)

	if err != nil {
		errorCode = http.StatusBadRequest
		errorText = fmt.Sprintf("Bad xml [%s]", err)
		// log.Println(errorText)
		return
	}

	if s.RequestXml.MethodName == "" {
		errorCode = http.StatusBadRequest
		errorText = "Haven't method"
		// log.Println(errorText)
		return
	}

	if len(s.RequestXml.Result) == 0 {
		errorCode = http.StatusBadRequest
		errorText = "Have not data"
		// log.Println(errorText)
		return
	}

	resultXML, errorCode, errorText = s.Worker.Work(s.RequestXml.MethodName, s.RequestXml.Result, r.URL.Query().Encode())
}
