package xmlrpc

import (
	"bytes"
	"errors"
	"fmt"
	"net/http"
	"time"
)

/*
Клиент для XMLRPC.
Параметры:
addr - адрес сервера XMLRPC;
methodName - имя метода;
data - данные для метода;
timeout - время жизни запроса.
*/
func Client(addr, methodName string, data []byte, timeout time.Duration) (response *http.Response, err error) {
	reader := bytes.NewReader(data)

	var req *http.Request
	req, err = http.NewRequest("POST", addr, reader)
	if err != nil {
		err = errors.New(fmt.Sprintf("Create request: %s", err))
		return
	}

	req.Header.Set("Content-Type", "text/xml")

	client := http.DefaultClient
	if timeout > 0 {
		client.Timeout = timeout
	}
	response, err = client.Do(req)

	if err != nil {
		err = errors.New(fmt.Sprintf("Create client, %s", err))
	}
	return
}
